package org.wit.android.helpers;

import android.util.Log;

/**
 * A helper utility class that provides assorted logger utility methods
 */
public class LogHelpers
{
  /**
   * Logs a message to the console
   *
   * @param parent
   * @param message
   */
    public static void info(Object parent, String message)
    {
        Log.i(parent.getClass().getSimpleName(), message);
    }
}
