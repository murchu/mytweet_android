package org.wit.mytweet.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;

import android.app.Fragment;
//import android.support.v4.app.Fragment;


import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.Manifest;
import android.content.pm.PackageManager;

import org.wit.android.helpers.ContactHelper;
import org.wit.android.helpers.IntentHelper;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;

import java.util.Date;
import java.util.UUID;

import static org.wit.android.helpers.ContactHelper.sendEmail;
import static org.wit.android.helpers.IntentHelper.navigateUp;
import static org.wit.android.helpers.LogHelpers.info;

/**
 * Class to govern all actions surrounding the sending of a tweet
 */
public class SendTweetFragment extends Fragment
    implements TextWatcher, View.OnClickListener
{
  public static final String EXTRA_TWEET_ID = "mytweet.TWEET_ID";
  private static final int REQUEST_CONTACT = 1;

  private TextView charCount;
  private Button sendTweet;
  private EditText tweetContent;
  private TextView date;
  private Button contact;
  private Button emailTweet;

  private TweetList tweetList;
  private Tweet tweet;

  MyTweetApp app;
  boolean newTweet = true;

  Intent data;

  /**
   * Governs rendering of send tweet view
   *
   * @param savedInstanceState
   */
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    UUID id = (UUID) getArguments().getSerializable(EXTRA_TWEET_ID);

    initialiseApp();

    if (id != null)
    {
      newTweet = false;
      tweet = tweetList.getTweet(id);
    }
    else
    {
      tweet = new Tweet("");
    }

  }

  /**
   * Helper method that initialises app
   */
  public void initialiseApp()
  {
    app = MyTweetApp.getApp();
    tweetList = app.tweetList;
    //MyTweetApp app = (MyTweetApp) getApplication();
    //tweetList = app.tweetList;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
  {
    super.onCreateView(inflater, parent, savedInstanceState);
    View view = inflater.inflate(R.layout.fragment_sendtweet, parent, false);

    //SendTweetPagerActivity sendTweetPagerActivity = (SendTweetPagerActivity) getActivity();
    //sendTweetPagerActivity.actionBar.setDisplayHomeAsUpEnabled(true);

    assignWidgets(view);
    initialiseValues();
    assignListeners();

    return view;
  }

  /**
   * Helper method that initialises all widgets on screen
   */
  public void assignWidgets(View v)
  {
    // assign all widgets
    charCount = (TextView) v.findViewById(R.id.charCount);
    tweetContent = (EditText) v.findViewById(R.id.tweetContent);
    sendTweet = (Button) v.findViewById(R.id.sendTweetButton);
    date = (TextView) v.findViewById(R.id.tweetDate);
    contact = (Button) v.findViewById(R.id.selectContactButton);
    emailTweet = (Button) v.findViewById(R.id.emailTweetButton);
  }

  /**
   * Helper method that assigns widget default values
   */
  public void initialiseValues()
  {
    // initialise widget default values
    charCount.setText(Tweet.length + "");
    tweetContent.setText(tweet.getContent());
    date.setText(new Date().toString());

    if (!newTweet)
    {
      tweetContent.setEnabled(false);
      sendTweet.setEnabled(false);
      charCount.setText("");
    }
  }

  /**
   * Helper method that assigns all listeners to widgets on screen
   */
  public void assignListeners()
  {
    // assign listeners
    if (newTweet)
    {
      tweetContent.addTextChangedListener(this);
      sendTweet.setOnClickListener(this);
    }

    contact.setOnClickListener(this);
    emailTweet.setOnClickListener(this);
  }

  /**
   * Overrides parent method to call selected action from menu
   *
   * @param item
   * @return
   */
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch(item.getItemId())
    {
      case android.R.id.home:
        navigateUp(getActivity());
        return true;

      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public void onPause()
  {
    super.onPause();
    tweetList.saveTweets();
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    if (resultCode != Activity.RESULT_OK)
    {
      return;
    }

    switch (requestCode)
    {
      case REQUEST_CONTACT:
        this.data = data;
        checkContactsReadPermission();
        break;
    }
  }

  private void readContact() {
    String name = ContactHelper.getContact(getActivity(), data);
    tweet.email = ContactHelper.getEmail(getActivity(), data);
    contact.setText(name + "\n<" + tweet.email + ">");
    emailTweet.setText("Email to " + name);
  }

  /**
   * http://stackoverflow.com/questions/32714787/android-m-permissions-onrequestpermissionsresult-not-being-called
   * This is an override of FragmentCompat.onRequestPermissionsResult
   *
   * @param requestCode Example REQUEST_CONTACT
   * @param permissions String array of permissions requested.
   * @param grantResults int array of results for permissions request.
   */
  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    switch (requestCode) {
      case REQUEST_CONTACT: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

          readContact();
        }
        break;
      }
    }
  }

  /**
   * Bespoke method to check if read contacts permission exists.
   * If it exists then the contact sought is read.
   * Otherwise, the method FragmentCompat.request permissions is invoked and
   * The response is via the callback onRequestPermissionsResult.
   * In onRequestPermissionsResult, on successfully being granted permission then the sought contact is read.
   */
  private void checkContactsReadPermission() {
    if (ContextCompat.checkSelfPermission(getActivity(),
        Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

      readContact();
    }
    else {
      // Invoke callback to request user-granted permission
      FragmentCompat.requestPermissions(
          this,
          new String[]{Manifest.permission.READ_CONTACTS},
          REQUEST_CONTACT);
    }
  }

  /**
   * TextWatcher listener
   *
   * @param s
   * @param start
   * @param count
   * @param after
   */
  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after)
  {
    //
  }

  /**
   * TextWatcher listener
   *
   * @param s
   * @param start
   * @param before
   * @param count
   */
  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count)
  {
    // changes char counter
    // TODO assess at removing to afterTextChanged with tweetContent
    int charsLeft = Tweet.length - s.length();
    charCount.setText("" + charsLeft);
    info(this, "New char length: " + s.length());
  }

  /**
   * TextWatcher listener
   *
   * @param s
   */
  @Override
  public void afterTextChanged(Editable s)
  {
    //tweet.setContent(s.toString());
    tweet.setContent(s.toString());
    info(this, "Current tweet string: " + s);

  }

  /**
   * View.onClickListener
   *
   * @param view
   */
  @Override
  public void onClick(View view)
  {
    switch (view.getId())
    {
      // tweet button clicks
      case R.id.sendTweetButton:
        if (!tweetList.containsTweet(tweet))
        {
          tweetList.addTweet(tweet);
        }
        tweetList.saveTweets();
        IntentHelper.startActivity(getActivity(), TweetListActivity.class);
        break;

      // select contact button clicks
      case R.id.selectContactButton:
        Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(i, REQUEST_CONTACT);
        break;

      // email button presses
      case R.id.emailTweetButton:
        // null pointer guard
        if(tweet.email == null)
        {
          tweet.email = "";
        }
        sendEmail(getActivity(), tweet.email, getString(R.string.emailTweetSubject), tweet.getEmailTweet(getActivity()));
        break;

    }
  }
}
