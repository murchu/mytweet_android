package org.wit.mytweet.app;

import android.app.Application;

import org.wit.mytweet.models.TweetList;
import org.wit.mytweet.models.TweetListSerializer;
import org.wit.mytweet.models.User;


/**
 * MyTweetApp class to initialise and manage app
 */
public class MyTweetApp extends Application
{
  public TweetList tweetList;
  private static final String FILENAME = "tweetlist.json";
  public DbHelper dbHelper = null;

  protected static MyTweetApp app;


  @Override
  public void onCreate()
  {
    super.onCreate();

    //initialise serializer and tweetlist upon app start
    TweetListSerializer serializer = new TweetListSerializer(this, FILENAME);
    tweetList = new TweetList(serializer);

    dbHelper = new DbHelper(getApplicationContext());

    app = this;
  }

  /**
   * Returns the MyTweet global app object
   *
   * @return
   */
  public static MyTweetApp getApp()
  {
    return app;
  }

  /**
   * Adds a new user to the SQL Lite db
   *
   * @param user
   */
  public void newUser(User user)
  {
    dbHelper.addUser(user);
  }

  /**
   * Validates a user against the details stored in the db
   *
   * @param email
   * @param password
   * @return
   */
  public boolean validUser (String email, String password)
  {
    User dbUser = dbHelper.selectUser(email);
    if (dbUser.password.equals(password)) {
      return true;
    }
    return false;
  }
}
