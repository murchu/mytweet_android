# MyTweet #

MyTweet is a twitter-like android application whereby a user can signup, login, send & email tweets, and view all tweets on a timeline.

## Dependencies ##

App requires a minimum android version of API 19, and has been designed with API 23 in mind.

## Accessing app ##

No login credentials are required prior to story tag st_05, where signup and login functionality is introduced. After story 05, the user will have to sign up through the app and login each time the user starts the app, as users are not persisted in any way up to and including story 10 (st_10). From story 11 (st_11) users are persisted via an SQL Lite db.

## Branches/ Structure ##

Repo follows a gitflow-like branch & merging structure, with the following branch types:

* features : ft1_xxx, ft2_xxx, etc
* dev : finished feature branches are merged back to dev
* master : finished development on dev is merged back to master

All work is done on individual feature branches, with finished features merged back to dev, and dev merged back to master when finished.

## Story Tags ##

Development of the app has been via the below functionality stories:

* st_01 : Adds basic tweet timeline view
* st_02 : Adds basic send tweet functions
* st_03 : Builds upon send tweet functionality
* st_04 : Adds JSON persistence for tweets
* st_05 : Adds welcome screen, with signup & login
* st_06 : Converts sendtweet & timeline into fragments
* st_07 : Implements email tweet functionality
* st_08 : Adds menus and settings
* st_09 : Adds ability to delete specific tweets
* st_10 : Adds ability to swipe through tweets
* st_11 : Implements SQL Lite for users
* st_12 : Completed version of app